## Microsoft's MVC Tutorial asp.net core 3.1 on ubuntu 18.04

An attempt to create the [MVC](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/start-mvc?view=aspnetcore-3.1&tabs=visual-studio-code) project from the tutorial on Microsoft's aspnet web site.

Check your versions [here](https://docs.microsoft.com/en-us/dotnet/core/install/linux-ubuntu).

For 18.04 the install looks like this:

```script
wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb

sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-3.1
```

Check your installation.

---

## Project creation.

```script
dotnet new mvc -o .
dotnet build
dotnet run
```
dotnet new mvc -o .

-o specifies output directory

. local gitlab directory

![ProjectCreation](Images/ProjectCreation.png)

---

## Controller and View Adds

Adding a controller and a view:

![ControllerAndView](Images/ViewControllerAdded.png)

Using emacs [magit](https://magit.vc/) on the top left of the image with [markdown mode](https://jblevins.org/projects/markdown-mode/) moddifing this README.md. Top right is the Microsoft tutorial, middle right is the app running in firefox. The bottom right is the service running from the command line.

I created a bit of a problem for myself, at this point in the project.

I wanted to pull the project from scratch from gitlab. So I thought... sure wipe the directory. Good idea.

Turns out I didn't add the wwwroot folder from project creation.

So I corrected the error and repulled the project from the command line. Then got a promt from magit for username/password pair.

Turns out I had pulled from the https source instead of the ssh.

```shell
git remote -v
```
If you've set up ssh automagic login you need [this](https://docs.github.com/en/github/using-git/changing-a-remotes-url):

```shell
git remote set-url origin git@github.com:USERNAME/REPOSITORY.git
```
I thought I'd mention it as a potental pitfall.

---

## Adding a Model

Successful package installation looks like [this](Images/PackageInstallation.png).

I had trouble a few days ago with the scaffolding tool.
Here is the [issue](https://github.com/dotnet/Scaffolding/issues/1384), currently 4-5 tickets open saying the same thing.

```script
dotnet tool uninstall --global dotnet-aspnet-codegenerator
dotnet tool install --global dotnet-aspnet-codegenerator --version 3.1.0
```

The newest version is broken.

Success looks like this:

![ModelScaffolding](Images/ModelScaffolding.png)

---

## Working with a database.

The connection string resides in the appsettings.json.

![Datbase Connection](Images/DatabaseConnection.png)

[DBeaver](https://dbeaver.io/) -- GUI sql 

Dbeaver doesn't have some easy to get to things like MS SQL Management Studio.

User creation comes to mind. Some simple to do tasks require more sql to make work within this type of editor.

Thinking about that, probably should just know the sql commands.

Problem is [this](https://stackoverflow.com/questions/1601186/sql-server-script-to-create-a-new-user). Complexity without the gui by design and need.

Just is the way it is.

---

## Controller Methods and Views

We go over alot of code but not really much is added to the project. 

The Movie model is given some data annotation tagging.

---

## Add Search

LINQ query from the Index.

![SearchFromIndex](Images/SearchFromIndex.png)

Modified Search for genre.

![SearchModded](Images/SearchModded.png)

---

## Add a New Field

I had a bit of trouble with the database at the end of this section.

```shell
andy@andyLinuxbox:~/dotnet/mvc$ rm MvcMovie.db 
andy@andyLinuxbox:~/dotnet/mvc$ dotnet ef database update
Build started...
Build succeeded.
Done.
andy@andyLinuxbox:~/dotnet/mvc$ dotnet build
Microsoft (R) Build Engine version 16.6.0+5ff7b0c9e for .NET Core
Copyright (C) Microsoft Corporation. All rights reserved.

  Determining projects to restore...
  All projects are up-to-date for restore.
  mvc -> /home/andy/dotnet/mvc/bin/Debug/netcoreapp3.1/mvc.dll
  mvc -> /home/andy/dotnet/mvc/bin/Debug/netcoreapp3.1/mvc.Views.dll

Build succeeded.
    0 Warning(s)
    0 Error(s)

Time Elapsed 00:00:01.10
andy@andyLinuxbox:~/dotnet/mvc$ dotnet run
fail: Microsoft.EntityFrameworkCore.Database.Command[20102]
      Failed executing DbCommand (1ms) [Parameters=[@p0='?' (Size = 15), @p1='?', @p2='?' (Size = 1), @p3='?', @p4='?' (Size = 20)], CommandType='Text', CommandTimeout='30']
      INSERT INTO "Movie" ("Genre", "Price", "Rating", "ReleaseDate", "Title")
      VALUES (@p0, @p1, @p2, @p3, @p4);
      SELECT "Id"
      FROM "Movie"
      WHERE changes() = 1 AND "rowid" = last_insert_rowid();
fail: Microsoft.EntityFrameworkCore.Update[10000]
An exception occurred in the database while saving changes for context type 'MvcMovie.Data.MvcMovieContext'.
      Microsoft.EntityFrameworkCore.DbUpdateException: An error occurred while updating the entries. See the inner exception for details.
       ---> Microsoft.Data.Sqlite.SqliteException (0x80004005): SQLite Error 1: 'table Movie has no column named Rating'.
```

I'll save you the rest of the stack dump.

The take away is in the last line there.

Here is how I fixed it:


```shell
andy@andyLinuxbox:~/dotnet/mvc$ rm MvcMovie.db 
andy@andyLinuxbox:~/dotnet/mvc$ rm Migrations/ -Rf
andy@andyLinuxbox:~/dotnet/mvc$ dotnet ef migrations add InitialCreate
Build started...
Build succeeded.
Done. To undo this action, use 'ef migrations remove'
andy@andyLinuxbox:~/dotnet/mvc$ dotnet ef database update
Build started...
Build succeeded.
Done.
andy@andyLinuxbox:~/dotnet/mvc$ dotnet build
Microsoft (R) Build Engine version 16.6.0+5ff7b0c9e for .NET Core
Copyright (C) Microsoft Corporation. All rights reserved.

  Determining projects to restore...
  All projects are up-to-date for restore.
  mvc -> /home/andy/dotnet/mvc/bin/Debug/netcoreapp3.1/mvc.dll
  mvc -> /home/andy/dotnet/mvc/bin/Debug/netcoreapp3.1/mvc.Views.dll

Build succeeded.
    0 Warning(s)
    0 Error(s)

Time Elapsed 00:00:01.11
andy@andyLinuxbox:~/dotnet/mvc$ dotnet run
info: Microsoft.Hosting.Lifetime[0]
      Now listening on: https://localhost:5001
info: Microsoft.Hosting.Lifetime[0]
      Now listening on: http://localhost:5000
info: Microsoft.Hosting.Lifetime[0]
      Application started. Press Ctrl+C to shut down.
info: Microsoft.Hosting.Lifetime[0]
      Hosting environment: Development
info: Microsoft.Hosting.Lifetime[0]
      Content root path: /home/andy/dotnet/mvc

```

This is a temp db. 

Migrations should be handled with care in other cases.

Here I just deleted it.

I might visit this with a [docker container](https://hub.docker.com/_/microsoft-mssql-server) later.

---

## Model Validation

The validation actually brought out a bug I had created for myself. I missed the Rating binding in the edit function and simply testing the validation helped me find it.

```c#
//MoviesController.cs

public async Task<IActionResult> Edit(int id, [Bind("Id,Title,ReleaseDate,Genre,Price,Rating")] Movie movie)
```
